import {
  Routes,
  Route,
  Link,
  Navigate,
} from "react-router-dom";
import { About } from "./About";
import './App.css';
import { Blog } from "./Blog";
import { ContactUs } from "./ContactUs";
import { Edit } from "./Edit";
import { Home } from './Home';

function App() {
  return (
    <div className="App">
           <header className="header">
            <ul className="d-flex">
                <li><Link to="/home">home</Link></li>
                <li><Link to="/about">about</Link></li>
                <li><Link to="/contactus">contact us</Link></li>
            </ul>
        </header>
      <Routes>
        <Route path="/" element={<Home/>}/> 
        <Route path="/home" element={<Navigate to="/"/>}/> 
        <Route path="blog/:id" element={<Blog/>}/> 
        <Route path="blog/edit/:id" element={<Edit/>}/>
        <Route path="about" element={<About/>}/> 
        <Route path="contactus" element={<ContactUs/>}/> 
        <Route path="/*" element={<Navigate replace to="/"/>}/> 
      </Routes>
      
    </div>
  );
}

export default App;
