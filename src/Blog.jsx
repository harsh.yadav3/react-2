import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Post } from './Home';
import { fetchdata } from './utlfnc/fnc';

export const Blog = () => {
    const {id} = useParams();
    const [data, setdata] = useState()
    const nav = useNavigate();

    useEffect(() => {
      fetchdata({fnc:setdata, id:id})    
    },[])
  return (
    <div className="post d-flex-c center m-top-s m-auto" style={{width: '70%'}}> 
    <div className="fulldetails">
        <h3 className="m-bottom-s ">{data?.title}</h3>
        <p>{data?.content}</p>
        <p className="m-top-m" style={{textAlign:"left"}}>:- {data?.author}</p>
    </div>
        <div className="btns m-top-m">
            <button onClick={()=>nav("/")} className = "  btn btn-1">back</button>
        </div>
    </div>
  )
}
