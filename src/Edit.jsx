import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { fetchdata, updatedata } from "./utlfnc/fnc";

export const Edit = () => {
  const {id} = useParams();
  const [data, setdata] = useState({
    title:"",
    content: "",
    author: "",
  })
  useEffect(() => {
    fetchdata({fnc:setdata,id:id})  
  }, []);
  const nav = useNavigate()


  function handleUpdate(e){
    e.preventDefault();
     console.log(data)
     updatedata(id,data) 
     nav("/")
  }
  
  return (
    <div className="edit m-top-x">
      <button className="btn btn-round" onClick={()=>nav("/")}>back</button>
      <form onSubmit={(e)=> handleUpdate(e)  } className="form d-flex-c">
        <label htmlFor="title">title</label>
        <input type="text" id="title" value={data.title} className="m-bottom-s" onChange={(e)=>setdata({...data,title:e.target.value})} required/>
        <label htmlFor="content">content</label>
        <textarea type="text" id="content" value={data.content} className="m-bottom-s" onChange={(e)=>setdata({...data,content:e.target.value})} required/>
        <label htmlFor="author">author</label>
        <input type="text" id="author" value={data.author} className="m-bottom-s" onChange={(e)=>setdata({...data,author:e.target.value})} required/>
        <button className="btn btn-1">submit</button>
      </form>
    </div>
  );
};
