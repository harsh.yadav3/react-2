import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { deletedata, fetchdata } from './utlfnc/fnc'

export const Home = () => {
    const [posts, setposts] = useState([])
    const [update, setupdate] = useState(false)
  
    useEffect(() => {
        fetchdata({fnc:setposts});
    }, [update])

    console.log(posts)
    


  return (
    <div className="home">
        <h2 className="text-c heading-1">Blogs</h2>
        <div className="posts m-auto ">
            {posts.map(post=>
                    <Post data={post} key={post.id} deldata={()=>{setupdate(!update);console.log(update)}}/>
                )}

        </div>
    </div>
  )
}

export const Post=({data,deldata}) =>{

    return(
        <div className="post d-flex-c center m-top-s"> 
        <div className="details">
            <h3 className="m-bottom-s">{data?.title}</h3>
            <p className="details__content">{data?.content}</p>
            <p className="m-top-m" style={{textAlign: 'left'}}>:- {data?.author}</p>
        </div>
            <div className="btns m-top-m">
                <Link to={`blog/edit/${data?.id}`} className="btn btn-1">edit</Link>
                <Link to={`blog/${data?.id}`} className="btn btn-1">show</Link>
                <button onClick={() => {deletedata(data?.id); deldata()}} className="btn btn-1 btn-1-red">delete</button>
            </div>
        </div>
    )
}
