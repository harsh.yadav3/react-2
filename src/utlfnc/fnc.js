export const fetchdata= async ({fnc,id})=>{
    const baseurl=`http://localhost:8000/posts`;
    const idurl=`http://localhost:8000/posts/${id}`;
    const posts = await (await fetch(id?idurl:baseurl)).json()
    fnc(posts)
}
export const updatedata= async (id,data)=>{
    const baseurl=`http://localhost:8000/posts/${id}`;
    const update = JSON.stringify(data);
    console.log(update)
    await fetch(baseurl,
        {
            method:'PUT',
            body:update,
            headers:{"Content-Type": "application/json"}
        })
}
export const deletedata= async (id)=>{
    const baseurl=`http://localhost:8000/posts/${id}`;    
    console.log((await fetch(baseurl,
        {
            method:'DELETE',
        })))
}